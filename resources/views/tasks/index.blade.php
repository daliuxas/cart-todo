@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">To do tasks <a href="/tasks/create" class="float-right btn btn-sm
                    btn-success">Add task</a>
                    </div>

                    @if(count($tasks) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach($tasks as $task)
                                @if(!$task->completed)
                                    <li class="list-group-item">
                                        <a href="/tasks/{{$task->id}}">{{$task->task_name}}</a>
                                        <form action="/tasks/{{$task->id}}" enctype="multipart/form-data" method="post"
                                              class="float-right">
                                            @csrf
                                            {{ method_field('PATCH') }}
                                            <input type="text" id="taskName" name="taskName"
                                                   value="{{$task->task_name}}" hidden>
                                            <input type="checkbox" id="completed" name="completed" checked hidden>
                                            <textarea id="taskDescription" rows="3"
                                                      name="taskDescription"
                                                      hidden>{{$task->task_description}}</textarea>
                                            <button type="submit" class="btn btn-sm btn-primary">Complete task</button>
                                        </form>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @else
                        <div class="card-body">
                            <p>No tasks found</p>
                        </div>
                    @endif


                </div>

                <div class="card mt-4">
                    <div class="card-header ">Completed tasks</div>
                    @if(count($tasks) > 0)
                        <ul class="list-group list-group-flush">
                            @foreach($tasks as $task)
                                @if($task->completed)
                                    <li class="list-group-item ">
                                        <a href="/tasks/{{$task->id}}">{{$task->task_name}}</a>
                                        <form action="/tasks/{{$task->id}}" enctype="multipart/form-data" method="post"
                                              class="float-right">
                                            @csrf
                                            {{ method_field('PATCH') }}
                                            <input type="text" id="taskName" name="taskName"
                                                   value="{{$task->task_name}}" hidden>
                                            <input type="checkbox" id="completed" name="completed" hidden>
                                            <textarea id="taskDescription" rows="3"
                                                      name="taskDescription"
                                                      hidden>{{$task->task_description}}</textarea>
                                            <button type="submit" class="btn btn-sm btn-secondary">Restore task</button>
                                        </form>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    @else
                        <div class="card-body">
                            <p>No tasks found</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
