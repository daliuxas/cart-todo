@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a Task</div>
                    <div class="card-body">
                        <form action="/tasks/{{$task->id}}" enctype="multipart/form-data" method="post">
                            @csrf
                            {{ method_field('PATCH') }}                            <div class="form-group">
                                <label for="taskName">Task name</label>
                                <input type="text" class="form-control" id="taskName" name="taskName"
                                       value="{{$task->task_name}}">
                            </div>
                            <div class="form-group">
                                <label for="taskDescription">Task description</label>
                                <textarea class="form-control" id="taskDescription" rows="3"
                                          name="taskDescription" >{{$task->task_description}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
