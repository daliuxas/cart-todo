@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a Task</div>
                    <div class="card-body">
                        <h5 class="card-title">{{$task->task_name}}</h5>
                        <p class="card-text">{{$task->task_description}}</p>
                    </div>
                    <div class="card-footer">
                        <a href="/tasks/{{$task->id}}/edit" class="float-left btn btn-sm btn-primary">Edit</a>
                        <form action="/tasks/{{$task->id}}" method="post">
                            {{ method_field('DELETE') }}
                            @csrf
                            <button class="float-right btn btn-sm btn-danger" type="submit">Delete</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
