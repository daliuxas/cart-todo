@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a Task</div>
                    <div class="card-body">
                        <form action="{{ route('tasks.store') }}" enctype="multipart/form-data" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="taskName">Task name</label>
                                <input type="text" class="form-control" id="taskName" name="taskName">
                            </div>
                            <div class="form-group">
                                <label for="taskDescription">Task description</label>
                                <textarea class="form-control" id="taskDescription" rows="3"
                                          name="taskDescription"></textarea>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
