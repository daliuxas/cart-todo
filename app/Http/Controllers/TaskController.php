<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $tasks = Task::where('user_id',$user_id)->orderBy('created_at','desc')->get();

        return view('tasks.index')->with('tasks', $tasks);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'taskName'        => 'required',
            'taskDescription' => 'required',
        ]);

        $task                   = new Task;
        $task->task_name        = $request->input('taskName');
        $task->task_description = $request->input('taskDescription');
        $task->user_id          = auth()->user()->id;
        $task->save();

        return redirect('/tasks')->with('success', 'task created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        if (auth()->user()->id !== $task->user_id) {
            return redirect('/tasks')->with('error', 'Unauthorised page');
        }

        return view('tasks.show')->with('task', $task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //check for correct user
        if (auth()->user()->id !== $task->user_id) {
            return redirect('/tasks')->with('error', 'Unauthorised page');
        }

        return view('tasks.edit')->with('task', $task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'taskName'        => 'required',
            'taskDescription' => 'required',
        ]);

        $task->task_name        = $request->input('taskName');
        $task->task_description = $request->input('taskDescription');
        $task->completed = $request->input('completed')?true:false;
        $task->save();

        return redirect('/tasks')->with('success', 'task updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task $task
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //check for correct user
        if (auth()->user()->id !== $task->user_id) {
            return redirect('/tasks')->with('error', 'Unauthorised page');
        }

        $task->delete();

        return redirect('/tasks')->with('success', 'Task Removed');
    }
}
